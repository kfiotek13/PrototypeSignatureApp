package hsx77;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.DoubleStream;

public class SignatureVerifier {

    // region Private Fields

    private static final double error = 0.1;

    private static Map<Integer, List<Double>> modelSpeedHistogram = new HashMap<>();
    private static Map<Integer, List<Double>> toVerifySpeedHistogram = new HashMap<>();

    //endregion

    //region Final Verifiers

    public static boolean VerifySignatureUsingManhattanAndEuclideanDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY, double[] modelSpeed, double[] speedToVerify, double maxTimeOfSignatureCreation, double minTimeOfSignatureCreation) {

        double similarityByTotalLength = CheckSimilarityOfSignaturesByTotalLengthUsingManhattanDistance(modelX, modelY, toCheckX, toCheckY);

        double similarityBySpeeds = CheckSimilarityOfSignaturesByHistogramsOfSpeedUsingEuclideanDistance(modelSpeed, speedToVerify, maxTimeOfSignatureCreation, minTimeOfSignatureCreation);

        if (similarityByTotalLength <= 70.0 && similarityBySpeeds <= 18.0)
            return true;
        else if (similarityByTotalLength <= 70.0 && similarityBySpeeds > 18.0) {
            if (similarityByTotalLength <= 40.0 && similarityBySpeeds <= 24.0)
                return true;
            else
                return false;
        } else if (similarityByTotalLength > 70.0 && similarityBySpeeds <= 18.0) {
            if (similarityByTotalLength <= 100.0 && similarityBySpeeds <= 15.0)
                return true;
            else
                return false;
        } else
            return false;
    }

    public static boolean VerifySignatureUsingOnlyEuclideanDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY, double[] modelSpeed, double[] speedToVerify, double maxTimeOfSignatureCreation, double minTimeOfSignatureCreation) {

        double similarityByTotalLength = CheckSimilarityOfSignaturesByTotalLengthUsingEuclideanDistance(modelX, modelY, toCheckX, toCheckY);

        double similarityBySpeeds = CheckSimilarityOfSignaturesByHistogramsOfSpeedUsingEuclideanDistance(modelSpeed, speedToVerify, maxTimeOfSignatureCreation, minTimeOfSignatureCreation);

        if (similarityByTotalLength <= 30.0 && similarityBySpeeds <= 18.0)
            return true;
        else if (similarityByTotalLength <= 30.0 && similarityBySpeeds > 18.0) {
            if (similarityByTotalLength <= 25.0 && similarityBySpeeds <= 24.0)
                return true;
            else
                return false;
        } else if (similarityByTotalLength > 30.0 && similarityBySpeeds <= 18.0) {
            if (similarityByTotalLength <= 60.0 && similarityBySpeeds <= 15.0)
                return true;
            else
                return false;
        } else
            return false;
    }

    private static double CheckSimilarityOfSignaturesByTotalLengthUsingManhattanDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double modelTotalLength = CalculateSumOfLengths(modelSignatureLengths);
        double toCheckTotalLength = CalculateSumOfLengths(resizedLengthsToCheck);
        return Math.abs(modelTotalLength - toCheckTotalLength);
    }

    private static double CheckSimilarityOfSignaturesByTotalLengthUsingEuclideanDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double modelTotalLength = CalculateSumOfLengths(modelSignatureLengths);
        double toCheckTotalLength = CalculateSumOfLengths(resizedLengthsToCheck);
        return Math.sqrt(Math.pow((modelTotalLength - toCheckTotalLength), 2));
    }

    private static double CheckSimilarityOfSignaturesByHistogramsOfSpeedUsingEuclideanDistance(double[] modelSpeed, double[] speedToVerify, double maxTimeOfSignatureCreation, double minTimeOfSignatureCreation) {

        double totalTimeOfSpeedToVerify = DoubleStream.of(speedToVerify).sum();
        if (totalTimeOfSpeedToVerify < minTimeOfSignatureCreation - minTimeOfSignatureCreation * error
                || totalTimeOfSpeedToVerify > maxTimeOfSignatureCreation + maxTimeOfSignatureCreation * error) {
            return 1000.00; // Such a great number of means lack of similarity
        }

        CreateHistogramOfModelSpeed(modelSpeed, speedToVerify);

        List<Double> euclideanSqrtElementsList = new ArrayList<>();

        for (Map.Entry<Integer, List<Double>> modelSpeedHist : modelSpeedHistogram.entrySet()) {

            double model = modelSpeedHist.getValue().size();
            double verify = toVerifySpeedHistogram.get(modelSpeedHist.getKey()).size();
            euclideanSqrtElementsList.add(Math.pow(model - verify, 2));
        }

        double euclideanDistance = 0.0;

        for (Double anEuclideanSqrtElementsList : euclideanSqrtElementsList) {
            euclideanDistance += anEuclideanSqrtElementsList;
        }
        return Math.sqrt(euclideanDistance);
    }

    //endregion

    //region Tests Methods

    //region Total lengths

    public static boolean CheckIsTheSameSignatureByTotalLengthUsingManhattan(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY, double errorThreshold) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double modelSumOfLengths = CalculateSumOfLengths(modelSignatureLengths);
        double toCheckSumOfLengths = CalculateSumOfLengths(resizedLengthsToCheck);
        return (Math.abs(modelSumOfLengths - toCheckSumOfLengths)) <= errorThreshold;
    }

    public static boolean CheckIsTheSameSignatureByTotalLengthUsingEuclidean(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY, double errorThreshold) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double modelSumOfLengths = CalculateSumOfLengths(modelSignatureLengths);
        double toCheckSumOfLengths = CalculateSumOfLengths(resizedLengthsToCheck);
        return Math.sqrt(Math.pow((modelSumOfLengths - toCheckSumOfLengths), 2)) <= errorThreshold;
    }

    // endregion

    //region Distance Between Points

    public static boolean CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY, double errorThreshold) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double manhattanDistance = 0.0;

        for (int i = 0; i < modelSignatureLengths.size(); i++) {
            manhattanDistance += Math.abs(modelSignatureLengths.get(i) - resizedLengthsToCheck.get(i));
        }
        return manhattanDistance <= errorThreshold;
    }

    public static boolean CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY, double errorThreshold) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double euclideanDistance = 0.0;

        for (int i = 0; i < modelSignatureLengths.size(); i++) {
            euclideanDistance += Math.pow((modelSignatureLengths.get(i) - resizedLengthsToCheck.get(i)), 2);
        }
        return Math.sqrt(euclideanDistance) <= errorThreshold;
    }

    public static boolean CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY,
                                                                                               double errorThreshold, int p, double r) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double minkowskiDistance = 0.0;

        for (int i = 0; i < modelSignatureLengths.size(); i++) {
            minkowskiDistance += Math.pow(Math.abs((modelSignatureLengths.get(i) - resizedLengthsToCheck.get(i))), r);
        }

        if (p == 1) {
            return minkowskiDistance <= errorThreshold;
        } else if (p == 2) {
            return Math.sqrt(minkowskiDistance) <= errorThreshold;
        } else {
            return Math.cbrt(minkowskiDistance) <= errorThreshold;
        }
    }

    //endregion

    //region Histogram of speeds

    public static boolean CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithoutTimeThreshold(double[] modelSpeed, double[] speedToVerify, double errorThreshold) {

        CreateHistogramOfModelSpeed(modelSpeed, speedToVerify);
        List<Double> euclideanSqrtElementsList = new ArrayList<>();

        for (Map.Entry<Integer, List<Double>> modelSpeedHist : modelSpeedHistogram.entrySet()) {

            double model = modelSpeedHist.getValue().size();
            double verify = toVerifySpeedHistogram.get(modelSpeedHist.getKey()).size();

            euclideanSqrtElementsList.add(Math.pow(model - verify, 2));
        }

        double euclideanDistance = 0.0;

        for (Double anEuclideanSqrtElement : euclideanSqrtElementsList) {
            euclideanDistance += anEuclideanSqrtElement;
        }
        return Math.sqrt(euclideanDistance) <= errorThreshold;
    }

    public static boolean CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(double[] modelSpeed, double[] speedToVerify,
      double maxTimeOfSignatureCreation, double minTimeOfSignatureCreation, double errorThreshold, double timeThreshold) {

        double totalTimeOfSpeedToVerify = DoubleStream.of(speedToVerify).sum();
        if (totalTimeOfSpeedToVerify < minTimeOfSignatureCreation - minTimeOfSignatureCreation * timeThreshold
                || totalTimeOfSpeedToVerify > maxTimeOfSignatureCreation + maxTimeOfSignatureCreation * timeThreshold) {
            return false;
        }

        CreateHistogramOfModelSpeed(modelSpeed, speedToVerify);

        List<Double> euclideanSqrtElementsList = new ArrayList<>();

        for (Map.Entry<Integer, List<Double>> modelSpeedHist : modelSpeedHistogram.entrySet()) {

            double model = modelSpeedHist.getValue().size();
            double verify = toVerifySpeedHistogram.get(modelSpeedHist.getKey()).size();

            euclideanSqrtElementsList.add(Math.pow(model - verify, 2));
        }

        double euclideanDistance = 0.0;

        for (Double anEuclideanSqrtElement : euclideanSqrtElementsList) {
            euclideanDistance += anEuclideanSqrtElement;
        }
        return Math.sqrt(euclideanDistance) <= errorThreshold;
    }

    //endregion

    //endregion

    //region Private Methods

    private static ArrayList<Double> CalculateLengthsBetweenPoints(double[] X, double[] Y) {
        ArrayList<Double> lengthsList = new ArrayList<>();

        for (int i = 0; i < X.length - 1; i++) {
            lengthsList.add(Math.hypot(X[i + 1] - X[i], Y[i + 1] - Y[i]));
        }
        return lengthsList;
    }

    private static double CalculateSumOfLengths(ArrayList<Double> lengthsListToSum) {
        double sum = 0;
        for (Double d : lengthsListToSum)
            sum += d;
        return sum;
    }

    private static void CreateHistogramOfModelSpeed(double[] modelSpeed, double[] speedToVerify) {
        double divisor = 15.0;

        double maxModelSpeed = GetMax(modelSpeed);
        double minModelSpeed = GetMin(modelSpeed);

        double differenceBetweenExtremeSizes = maxModelSpeed - minModelSpeed;
        double section = differenceBetweenExtremeSizes / divisor;

        double temp = -1.0;
        List<Double> histValues;
        List<Double> histValues2;

        for (int i = 1; i <= divisor; i++) {

            histValues = new ArrayList<>();
            histValues2 = new ArrayList<>();

            double newTemp = temp + section;

            for (double a : modelSpeed) {
                if (a > temp && a <= newTemp) {
                    histValues.add(a);
                }
            }

            for (double a : speedToVerify) {
                if (a > temp && a <= newTemp) {
                    histValues2.add(a);
                }
            }
            modelSpeedHistogram.put(i, histValues);
            toVerifySpeedHistogram.put(i, histValues2);

            temp = newTemp;
        }
    }

    private static ArrayList<Double> ResizeLengths(ArrayList<Double> modelLengthsList, ArrayList<Double> lengthsToCheckList) {
        for (int i = 0; i < modelLengthsList.size(); i++) {

            double resizedLengthToCheck = 0.0;
            double lengthToCheck = lengthsToCheckList.get(i);

            if (lengthToCheck != 0.0)
                resizedLengthToCheck = lengthToCheck * (modelLengthsList.get(i) / lengthToCheck);

            lengthsToCheckList.set(i, resizedLengthToCheck);
        }
        return lengthsToCheckList;
    }

    private static double GetMax(double[] inputArray) {
        double maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    private static double GetMin(double[] inputArray) {
        double minValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] < minValue) {
                minValue = inputArray[i];
            }
        }
        return minValue;
    }

    //endregion
}
