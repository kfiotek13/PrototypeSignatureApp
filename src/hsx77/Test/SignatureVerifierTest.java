package hsx77.Test;

import hsx77.SignatureManipulation;
import hsx77.SignatureVerifier;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

class SignatureVerifierTest {

    private static final double totalLengthsErrorThreshold = 70.0, distanceBetweenPointsErrorThreshold = 20.0,
            speedOfWritingErrorThreshold = 18.0, timeThreshold = 0.1;

    //region Final Verifiers

    // region Final Verification using Manhattan and Euclidean distance

    private String testedUser = "U20";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_UsingManhattanAndEuclideanDistance_ByTotalVerifiers(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_UsingManhattanAndEuclideanDistance_ByTotalVerifiers(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    private String otherUser = "U1";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanAndEuclideanDistance_ByTotalVerifiers_1(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanAndEuclideanDistance_ByTotalVerifiers_7(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanAndEuclideanDistance_ByTotalVerifiers_15(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanAndEuclideanDistance_ByTotalVerifiers_21(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanAndEuclideanDistance_ByTotalVerifiers_32(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    //endregion

    // region Final Verification using only Euclidean distance

    //  private String testedUser = "U34";
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_UsingOnlyEuclideanDistance_ByTotalVerifiers(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_UsingOnlyEuclideanDistance_ByTotalVerifiers(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    //  private String otherUser = "U1";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingOnlyEuclideanDistance_ByTotalVerifiers_1(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingOnlyEuclideanDistance_ByTotalVerifiers_7(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingOnlyEuclideanDistance_ByTotalVerifiers_15(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingOnlyEuclideanDistance_ByTotalVerifiers_21(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingOnlyEuclideanDistance_ByTotalVerifiers_32(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
    }

    //endregion

//endregion

    //region Total lengths

    // region Functionality tests related to total lengths using Manhattan Distance

    //   private String testedUser = "U4";
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_BySumOfLengths(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_BySumOfLengths(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    //  private String otherUser = "U1";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths7(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths15(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths21(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths32(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    //endregion

    // region Functionality tests related to total lengths using Euclidean Distance

    //  private String testedUser = "U24";
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_BySumOfLengthsByEuclidean(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_BySumOfLengthsByEuclidean(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    // private String otherUser = "U1";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengthsByEuclidean(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths7ByEuclidean(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths15ByEuclidean(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths21ByEuclidean(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_BySumOfLengths32ByEuclidean(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), totalLengthsErrorThreshold));
    }

    //endregion

    //endregion

    //region Distance between points

    // region Functionality tests related to distance between points using Manhattan Distance

    //  private String testedUser = "U17";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_ByDistanceBetweenPointsUsingManhattanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_ByDistanceBetweenPointsUsingManhattanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    // private String otherUser = "U1";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_ByDistanceBetweenPointsUsingManhattanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }


    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser7_ByDistanceBetweenPointsUsingManhattanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser15_ByDistanceBetweenPointsUsingManhattanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser21_ByDistanceBetweenPointsUsingManhattanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser32_ByDistanceBetweenPointsUsingManhattanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

//endregion

    // region Functionality tests related to distance between points using Euclidean Distance

    // private String testedUser = "U37";

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_ByDistanceBetweenPointsUsingEuclideanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_ByDistanceBetweenPointsUsingEuclideanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    //    private String otherUser = "U1";
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_ByDistanceBetweenPointsUsingEuclideanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }


    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser7_ByDistanceBetweenPointsUsingEuclideanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser15_ByDistanceBetweenPointsUsingEuclideanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser21_ByDistanceBetweenPointsUsingEuclideanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser32_ByDistanceBetweenPointsUsingEuclideanDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold));
    }

//endregion

    // region Functionality tests related to distance between points using Minkowski Distance

    //private String testedUser = "U15";

    private static int p = 2;
    private static double r = 2.0d;

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_ByDistanceBetweenPointsUsingMinkowskiDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold, p, r));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_ByDistanceBetweenPointsUsingMinkowskiDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold, p, r));
    }

    //    private String otherUser = "U1";
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_ByDistanceBetweenPointsUsingMinkowskiDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold, p, r));
    }


    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser7_ByDistanceBetweenPointsUsingMinkowskiDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold, p, r));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser15_ByDistanceBetweenPointsUsingMinkowskiDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold, p, r));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser21_ByDistanceBetweenPointsUsingMinkowskiDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold, p, r));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser32_ByDistanceBetweenPointsUsingMinkowskiDistance(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), distanceBetweenPointsErrorThreshold, p, r));
    }

//endregion

//endregion

    //region Histogram of speeds

    // region Functionality tests related to speeds using Euclidean Distance

    //private String testedUser = "U16";
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20})
    void Should_ReturnTrue_IfTestedSignatureIsOriginal_UsingHistogramsOfSpeed(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertTrue(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), speedOfWritingErrorThreshold, timeThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsForged_UsingHistogramOfSpeed(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);
        SignatureManipulation signatureToVerify = new SignatureManipulation(testedUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), speedOfWritingErrorThreshold, timeThreshold));
    }

    // private String otherUser = "U1";
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingHistogramOfSpeed(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), speedOfWritingErrorThreshold, timeThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser7_UsingHistogramOfSpeed(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U7", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), speedOfWritingErrorThreshold, timeThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser15_UsingHistogramOfSpeed(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U15", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), speedOfWritingErrorThreshold, timeThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser21_UsingHistogramOfSpeed(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U21", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), speedOfWritingErrorThreshold, timeThreshold));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})
    void Should_ReturnFalse_IfTestedSignatureIsFromOtherUser32_UsingHistogramOfSpeed(int numberOfSignatureToCheck) {

        SignatureManipulation modelSignature = new SignatureManipulation(testedUser, 20);

        SignatureManipulation signatureToVerify = new SignatureManipulation("U32", numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());

        assertFalse(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), speedOfWritingErrorThreshold, timeThreshold));
    }

//endregion

//endregion
}

