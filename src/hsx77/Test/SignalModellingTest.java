package hsx77.Test;

import hsx77.SignalModelling;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertArrayEquals;

class SignalModellingTest {


    @Test
    void ShouldNormalizeData() {
        double[] initialData = {1, 17, 22, 308, 7, 35, 23, 709, 2, 11};

        double[] result = SignalModelling.NormalizeData(initialData, 1000);

        double[] expectedResult = {0.0, 22.598870056497177, 29.661016949152543, 433.61581920903956, 8.474576271186441,
                48.0225988700565, 31.073446327683616, 1000.0, 1.41242937853107, 14.1242937853107};

        assertArrayEquals(expectedResult, result, 2);
    }

    @Test
    void ShouldResampleData() {

        double[] initialData = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
        double[] expectedResult = {6.85116786691568 * Math.exp(-19), 0.0593837503936124, 0.138815659981342, 0.200121234710756, 0.261237935751573
                , 0.340380889053760, 0.400242469421511, 0.455368496988410, 0.548959082928986, 0.600363704132267, 0.640239421685762
                , 0.769646943537187, 0.800484938843022, 0.797610545450127, 1.04171978196731, 1.00060617355378, 0.315303688543327};

        PolynomialSplineFunction interpolatedData = SignalModelling.InterpolateData(initialData);

        double[] result = SignalModelling.ResampleData(17, 11, interpolatedData);

        assertArrayEquals(expectedResult, result, 2);
    }
}