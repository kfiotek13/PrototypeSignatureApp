package hsx77;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Polyline;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MainController implements Initializable {


    private SignatureManipulation modelSignature;
    private String selectedUser;
    private String userToCheck;
    private int numberOfSignatureToCheck;

    @FXML
    public Button btnChooseUser, btnTests;
    public Pane pnUpperRight, pnLowerRight, pnResult;
    public ComboBox cbxUserSelection, cbxSignatureToCheck, cbxNumberOfSignature;
    public TextField txtAnswer;
    public BorderPane brdPane;


    @FXML
    public void btnChooseUser() {
        Polyline polyline = new Polyline();

        if (selectedUser != null && !selectedUser.isEmpty()) {
            pnUpperRight.getChildren().clear();
            modelSignature = new SignatureManipulation(selectedUser, 20);
            polyline.getPoints().addAll(modelSignature.GetModelXYList());

            polyline.setLayoutX(-250);
            polyline.setLayoutY(-370);

            polyline.scaleXProperty().setValue(0.25);
            polyline.scaleYProperty().setValue(0.25);
            pnUpperRight.getChildren().add(polyline);

        } else
            new Alert(Alert.AlertType.WARNING, "Select a user!").showAndWait();
    }

    @FXML
    public void btnCheckSignature() {

        if (userToCheck != null && numberOfSignatureToCheck > 0 && !userToCheck.isEmpty() && modelSignature != null) {
            pnLowerRight.getChildren().clear();

            SignatureManipulation signatureToVerify = new SignatureManipulation(userToCheck, numberOfSignatureToCheck, modelSignature.GetMaxSignatureLength());
            Polyline polyline = new Polyline();
            polyline.getPoints().addAll(signatureToVerify.GetXYToCheck());

            polyline.setLayoutX(-250);
            polyline.setLayoutY(-370);

            polyline.scaleXProperty().setValue(0.25);
            polyline.scaleYProperty().setValue(0.25);
            pnLowerRight.getChildren().add(polyline);

            if (SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(),
                    modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation())) {
                pnResult.setStyle("-fx-background-color: green;");
                txtAnswer.setVisible(true);
                txtAnswer.setStyle("-fx-background-color: green;");
                txtAnswer.setText("Correct authentication");
            } else {
                pnResult.setStyle("-fx-background-color: red;");
                txtAnswer.setVisible(true);
                txtAnswer.setStyle("-fx-background-color: red;");
                txtAnswer.setText("Invalid authentication");
            }
        } else
            new Alert(Alert.AlertType.WARNING, "Select a user!").showAndWait();
    }

    @FXML
    public void cmbGetSelectedUser() {
        selectedUser = cbxUserSelection.getValue().toString();
    }

    @FXML
    public void cmbGetUserToCheck() {
        userToCheck = cbxSignatureToCheck.getValue().toString();
    }

    @FXML
    public void cmbGetNumberOfSignature() {
        numberOfSignatureToCheck = (int) cbxNumberOfSignature.getValue();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        InitializeComboBoxesDataSet();
        txtAnswer.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    private void InitializeComboBoxesDataSet() {
        ArrayList UsersDataSet = new ArrayList();

        for (int i = 1; i <= 40; i++) {
            StringBuilder sB = new StringBuilder("U");
            sB.append(i);
            UsersDataSet.add(sB);
            cbxNumberOfSignature.getItems().add(i);
        }
        cbxUserSelection.getItems().addAll(UsersDataSet);
        cbxSignatureToCheck.getItems().addAll(UsersDataSet);
    }

    @FXML
    public void btnTestsClick(ActionEvent actionEvent) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("verifierTestsFrame.fxml"));
        Scene testScene = new Scene(parent);
        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(testScene);
        window.show();
    }
}
