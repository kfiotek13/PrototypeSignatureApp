package hsx77;

import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.DoubleStream;

import static java.lang.Double.parseDouble;

public class SignatureManipulation {

    //region Private Fields

    private double[] X;
    private double[] Y;
    private double[] Speed;

    private double[] modelAvgSignatureX;
    private double[] modelAvgSignatureY;
    private double[] modelAvgSpeedOfSignature;

    private double[] signatureToVerifyX;
    private double[] signatureToVerifyY;
    private double[] signatureSpeedToVerify;

    private int numberOfUserSignatures;
    private int maxSignatureLength;

    private double maxTimeOfSignatureCreation;
    private double minTimeOfSignatureCreation;


    private ArrayList<Integer> signaturePointsList = new ArrayList<>();
    private SignatureSamplesContainer signatureContainer;

    //endregion Private Fields

    //region Constructors

    public SignatureManipulation(String selectedUser, int numberOfUserSignatures) {

        maxSignatureLength = 0;
        this.numberOfUserSignatures = numberOfUserSignatures;
        signatureContainer = new SignatureSamplesContainer();

        LoadUsersSignatures(selectedUser);
        ResampleSignatures();
        CreateModelSignature();

        SetMaxAndMinTimeOfWritingSignature();
    }

    public SignatureManipulation(String selectedUser, int signatureNumber, int maxSignatureLength) {
        this.maxSignatureLength = maxSignatureLength;

        signatureContainer = new SignatureSamplesContainer();
        LoadSignature(selectedUser, signatureNumber);
        PrepareSignatureToVerify();
    }

    //endregion Constructors

    //region Private Methods

    private void LoadSignature(String userName, int numberOfSignature) {

        File f = new File("src/SignaturesDataSet/" + userName + "S" + numberOfSignature + ".TXT");
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {

            String currentLine;
            int currentSignaturePoints = Integer.parseInt(br.readLine());

            if (maxSignatureLength < currentSignaturePoints)
                maxSignatureLength = currentSignaturePoints;

            signaturePointsList.add(currentSignaturePoints);
            X = new double[currentSignaturePoints];
            Y = new double[currentSignaturePoints];
            Speed = new double[currentSignaturePoints];

            int i = 0;
            while ((currentLine = br.readLine()) != null) {
                String[] stringParts = currentLine.split(" ");

                X[i] = parseDouble(stringParts[0]);
                Y[i] = parseDouble(stringParts[1]);
                Speed[i] = parseDouble(stringParts[2]);
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void LoadUsersSignatures(String userName) {
        for (int i = 1; i <= numberOfUserSignatures; i++) {
            LoadSignature(userName, i);
            signatureContainer.SetXSignatureSample(i, X);
            signatureContainer.SetYSignatureSample(i, Y);
            signatureContainer.SetSpeedOfSignatureSample(i, Speed);
        }
    }

    private void ResampleSignatures() {
        for (int i = 1; i <= numberOfUserSignatures; i++) {

            PolynomialSplineFunction xSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(signatureContainer.GetXSignatureSample(i), 1000));
            PolynomialSplineFunction ySignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(signatureContainer.GetYSignatureSample(i), 1000));
            PolynomialSplineFunction speedSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(signatureContainer.GetSpeedOfSignatureSample(i), 1000));

            signatureContainer.SetResampledXSignatureSample(i, SignalModelling.ResampleData(maxSignatureLength, signaturePointsList.get(i - 1), xSignalToResample));
            signatureContainer.SetResampledYSignatureSample(i, SignalModelling.ResampleData(maxSignatureLength, signaturePointsList.get(i - 1), ySignalToResample));
            signatureContainer.SetResampledSpeedOfSignatureSample(i, SignalModelling.ResampleData(maxSignatureLength, signaturePointsList.get(i - 1), speedSignalToResample));
        }
    }

    private void CreateModelSignature() {

        double[] tempSameXCoordinatesTable = new double[numberOfUserSignatures];
        double[] tempSameYCoordinatesTable = new double[numberOfUserSignatures];
        double[] tempSameSpeedCoordinatesTable = new double[numberOfUserSignatures];

        modelAvgSignatureX = new double[maxSignatureLength];
        modelAvgSignatureY = new double[maxSignatureLength];
        modelAvgSpeedOfSignature = new double[maxSignatureLength];

        for (int j = 0; j < maxSignatureLength; j++) { // For each coordinate of signature

            for (int i = 1; i <= numberOfUserSignatures; i++) { // For each sample of signature of the chosen user

                double[] resampledXSignature = signatureContainer.GetResampledXSignatureSample(i); // Resampled samples of a next sample of signature
                double[] resampledYSignature = signatureContainer.GetResampledYSignatureSample(i);
                double[] resampledSpeedOfSignature = signatureContainer.GetResampledSpeedOfSignatureSample(i);

                tempSameXCoordinatesTable[i - 1] = resampledXSignature[j]; //Fill temp array by ith sample of each signature
                tempSameYCoordinatesTable[i - 1] = resampledYSignature[j];
                tempSameSpeedCoordinatesTable[i - 1] = resampledSpeedOfSignature[j];
            }
            modelAvgSignatureX[j] = GetAverageValue(tempSameXCoordinatesTable); // GetMedianValue(tempSameXCoordinatesTable);
            modelAvgSignatureY[j] = GetAverageValue(tempSameYCoordinatesTable); // GetMedianValue(tempSameYCoordinatesTable);
            modelAvgSpeedOfSignature[j] = GetAverageValue(tempSameSpeedCoordinatesTable);
        }
    }

    private double GetAverageValue(double[] coordinatesTable) {
        return (DoubleStream.of(coordinatesTable).sum()) / numberOfUserSignatures;
    }

    private double GetMedianValue(double[] numArray) {
        Arrays.sort(numArray);
        if (numArray.length % 2 == 0)
            return (numArray[numArray.length / 2] + numArray[numArray.length / 2 - 1]) / 2;
        else
            return numArray[numArray.length / 2];
    }

    private void PrepareSignatureToVerify() {
        PolynomialSplineFunction xSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(X, 1000));
        PolynomialSplineFunction ySignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(Y, 1000));
        PolynomialSplineFunction speedSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(Speed, 1000));

        signatureToVerifyX = SignalModelling.ResampleData(maxSignatureLength, X.length, xSignalToResample);
        signatureToVerifyY = SignalModelling.ResampleData(maxSignatureLength, Y.length, ySignalToResample);
        signatureSpeedToVerify = SignalModelling.ResampleData(maxSignatureLength, Speed.length, speedSignalToResample);
    }

    private void SetMaxAndMinTimeOfWritingSignature() {
        maxTimeOfSignatureCreation = minTimeOfSignatureCreation =  //min/max initialization
                DoubleStream.of(SignalModelling.NormalizeData(signatureContainer.GetSpeedOfSignatureSample(1), 1000)).sum();

        for (int i = 1; i <= numberOfUserSignatures; i++) {

            double[] signatureSpeed = SignalModelling.NormalizeData(signatureContainer.GetSpeedOfSignatureSample(i), 1000);
            double sumOfSignatureSpeeds = DoubleStream.of(signatureSpeed).sum();

            if (sumOfSignatureSpeeds > maxTimeOfSignatureCreation)
                maxTimeOfSignatureCreation = sumOfSignatureSpeeds;
            if (sumOfSignatureSpeeds < minTimeOfSignatureCreation)
                minTimeOfSignatureCreation = sumOfSignatureSpeeds;
        }
    }

    //endregion Private Methods

    //region Properties

    public int GetMaxSignatureLength() {
        return maxSignatureLength;
    }

    public double[] GetModelAvgSignatureX() {
        return modelAvgSignatureX;
    }

    public double[] GetModelAvgSignatureY() {
        return modelAvgSignatureY;
    }

    public double[] GetModelAvgSpeedOfSignature() {
        return modelAvgSpeedOfSignature;
    }

    public ArrayList<Double> GetModelXYList() {
        ArrayList<Double> XY = new ArrayList<>();
        for (int i = 0; i < maxSignatureLength; i++) {
            XY.add(modelAvgSignatureX[i]);
            XY.add(modelAvgSignatureY[i]);
        }
        return XY;
    }

    public double[] GetSignatureToVerifyX() {
        return signatureToVerifyX;
    }

    public double[] GetSignatureToVerifyY() {
        return signatureToVerifyY;
    }

    public double[] GetSpeedOfSignatureToVerify() {
        return signatureSpeedToVerify;
    }

    public ArrayList<Double> GetXYToCheck() {
        ArrayList<Double> XYToCheck = new ArrayList<>();
        for (int i = 0; i < signatureToVerifyX.length; i++) {
            XYToCheck.add(signatureToVerifyX[i]);
            XYToCheck.add(signatureToVerifyY[i]);
        }
        return XYToCheck;
    }

    public double GetMaxTimeOfSignatureCreation() {
        return maxTimeOfSignatureCreation;
    }

    public double GetMinTimeOfSignatureCreation() {
        return minTimeOfSignatureCreation;
    }

    //endregion
}
