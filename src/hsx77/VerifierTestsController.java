package hsx77;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class VerifierTestsController implements Initializable {


    @FXML
    public BorderPane brdPaneTitle;
    public ComboBox cbxAuthenticatedUser, cbxOtherUser;
    public RadioButton rbGenuineSignature, rbForgedSignature, rbOtherSignature, rbP1, rbP2, rbP3, rbR1, rbR2, rbR3;
    public Button btnMain, btn_TL_Mt, btn_TL_Ec, btn_DP_Mt, btn_DP_Ec, btn_DP_Mk, btn_SW, btn_SW_TT, btn_TV_Mt, btn_TV_Ec;
    public TextField txtTL_Mt_result, txtTL_Ec_result, txtTL_Mt_error, txtTL_Ec_error, txtDP_Mt_error,
            txtDP_Ec_error, txtDP_Mk_error, txtDP_Mt_result, txtDP_Ec_result, txtDP_Mk_result, txtSW_error,
            txtSW_TT_error, txtSW_TT_timeError, txtSW_result, txtSW_TT_result, txtTV_Mt_error, txtTV_Ec_result,
            txtTV_Ec_error, txtTV_Mt_result;
    private ToggleGroup mainRadioButtonGroup, pRadioButtonGroup, rRadioButtonGroup;

    // Private Fields
    private static int numberOfUsers = 20, allUsers = 40;
    private String authenticatedUser, otherUser;
    private ArrayList OtherUsersDataSet;
    private double errorThreshold, timeThreshold, r;
    private int p;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        InitializeAuthenticatedUserComboBoxDataSet();
        InitializeMainRadioButtons();
        InitializePRadioButtons();
        InitializeRRadioButtons();

        SetOtherUserComboBoxDataSet();

        SetMainRadioButtonGroup();
        SetPRadioButtonGroup();
        SetRRadioButtonGroup();

        cbxAuthenticatedUser.getSelectionModel().select(0);
        rbGenuineSignature.setSelected(true);
        authenticatedUser = cbxAuthenticatedUser.getValue().toString();
    }

    @SuppressWarnings("unchecked")
    private void InitializeAuthenticatedUserComboBoxDataSet() {
        ArrayList UsersDataSet = new ArrayList();

        for (int i = 1; i <= 40; i++) {
            StringBuilder sB = new StringBuilder("U");
            sB.append(i);
            UsersDataSet.add(sB);
        }
        cbxAuthenticatedUser.getItems().addAll(UsersDataSet);
    }

    @SuppressWarnings("unchecked")
    private void SetOtherUserComboBoxDataSet() {

        if (OtherUsersDataSet == null) {
            OtherUsersDataSet = new ArrayList();
            OtherUsersDataSet.addAll(Arrays.asList("U1", "U7", "U15", "U21", "U32"));
        }
        cbxOtherUser.getItems().addAll(OtherUsersDataSet);
    }

    private void SetMainRadioButtonGroup() {
        mainRadioButtonGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {

            if (rbGenuineSignature.isSelected())
                cbxOtherUser.setDisable(true);
            if (rbForgedSignature.isSelected())
                cbxOtherUser.setDisable(true);
            if (rbOtherSignature.isSelected()) {
                cbxOtherUser.setDisable(false);
                cbxOtherUser.getSelectionModel().select(1);
                otherUser = cbxOtherUser.getValue().toString();
            }
        });
    }

    private void SetPRadioButtonGroup() {
        pRadioButtonGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {

            if (rbP1.isSelected())
                p = 1;
            if (rbP2.isSelected())
                p = 2;
            if (rbP3.isSelected())
                p = 3;
        });
    }

    private void SetRRadioButtonGroup() {
        rRadioButtonGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {

            if (rbR1.isSelected())
                r = 1.0d;
            if (rbR2.isSelected())
                r = 2.0d;
            if (rbR3.isSelected())
                r = 3.0d;
        });
    }

    private void InitializeMainRadioButtons() {
        mainRadioButtonGroup = new ToggleGroup();

        RadioButton rb1 = rbGenuineSignature;
        rb1.setToggleGroup(mainRadioButtonGroup);

        RadioButton rb2 = rbForgedSignature;
        rb2.setToggleGroup(mainRadioButtonGroup);

        RadioButton rb3 = rbOtherSignature;
        rb3.setToggleGroup(mainRadioButtonGroup);
    }

    private void InitializePRadioButtons() {
        pRadioButtonGroup = new ToggleGroup();

        RadioButton rb1 = rbP1;
        rb1.setToggleGroup(pRadioButtonGroup);

        RadioButton rb2 = rbP2;
        rb2.setToggleGroup(pRadioButtonGroup);

        RadioButton rb3 = rbP3;
        rb3.setToggleGroup(pRadioButtonGroup);

        rbP2.setSelected(true);
        p = 2;
    }

    private void InitializeRRadioButtons() {
        rRadioButtonGroup = new ToggleGroup();

        RadioButton rb1 = rbR1;
        rb1.setToggleGroup(rRadioButtonGroup);

        RadioButton rb2 = rbR2;
        rb2.setToggleGroup(rRadioButtonGroup);

        RadioButton rb3 = rbR3;
        rb3.setToggleGroup(rRadioButtonGroup);

        rbR2.setSelected(true);
        r = 2.0d;
    }

    public void cbxGetAuthenticatedUser() {
        authenticatedUser = cbxAuthenticatedUser.getValue().toString();
    }

    public void cbxGetOtherUser() {
        otherUser = cbxOtherUser.getValue().toString();
    }

    public void btnMainClick(ActionEvent actionEvent) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("mainFrame.fxml"));
        Scene testScene = new Scene(parent);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(testScene);
        window.show();
    }

    private int CountCorrectResultsOfTest(ArrayList<Boolean> resultsList) {
        int count = 0;
        for (Boolean correct : resultsList) {
            if (correct) {
                count++;
            }
        }
        return count;
    }

    private boolean CheckAndSetErrorOrTimeThresholdValue(String errorThresholdValue, boolean isTimeThreshold, TextField resultTextField) {

        if (errorThresholdValue.isEmpty()) {
            if (isTimeThreshold)
                new Alert(Alert.AlertType.WARNING, "Set time threshold value.").showAndWait();
            else
                new Alert(Alert.AlertType.WARNING, "Set error threshold value.").showAndWait();

            ClearResultTextFieldIfNotEmpty(resultTextField);
            return false;
        }

        String errorThresholdToParse = errorThresholdValue.replace(",", ".");

        try {
            if (isTimeThreshold)
                timeThreshold = Double.parseDouble(errorThresholdToParse) / 100;
            else
                errorThreshold = Double.parseDouble(errorThresholdToParse);
            return true;
        } catch (Exception exception) {
            if (isTimeThreshold)
                new Alert(Alert.AlertType.WARNING, "Set correct time threshold value.").showAndWait();
            else
                new Alert(Alert.AlertType.WARNING, "Set correct error threshold value.").showAndWait();

            ClearResultTextFieldIfNotEmpty(resultTextField);
            return false;
        }
    }

    private boolean CheckIfAuthenticatedUserIsTheSameAsOtherUser(TextField resultTextField) {
        if (authenticatedUser.equals(otherUser)) {
            new Alert(Alert.AlertType.WARNING, "Other user must be different than the authenticated user.").showAndWait();
            ClearResultTextFieldIfNotEmpty(resultTextField);
            return false;
        }
        return true;
    }

    private void ClearResultTextFieldIfNotEmpty(TextField resultTextField) {
        if (!resultTextField.getText().isEmpty())
            resultTextField.setText("");
    }

    //region Total Lengths

    //region Manhattan Distance

    public void btn_TL_MtClick() {
        if (CheckAndSetErrorOrTimeThresholdValue(txtTL_Mt_error.getText(), false, txtTL_Mt_result)) {
            if (rbGenuineSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingManhattanDistance_ByTotalLength();
                int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
                txtTL_Mt_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbForgedSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingManhattanDistance_ByTotalLength();
                int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtTL_Mt_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbOtherSignature.isSelected()) {
                if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtTL_Mt_result)) {
                    ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanDistance_ByTotalLength();
                    int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                    txtTL_Mt_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
                }
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingManhattanDistance_ByTotalLength() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingManhattanDistance_ByTotalLength() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(),
                    modelSignature.GetModelAvgSignatureY(), signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanDistance_ByTotalLength() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingManhattan(modelSignature.GetModelAvgSignatureX(),
                    modelSignature.GetModelAvgSignatureY(), signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    //    endregion

    //region Euclidean Distance

    public void btn_TL_EcClick() {
        if (CheckAndSetErrorOrTimeThresholdValue(txtTL_Ec_error.getText(), false, txtTL_Ec_result)) {
            if (rbGenuineSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingEuclideanDistance_ByTotalLength();
                int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
                txtTL_Ec_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbForgedSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingEuclideanDistance_ByTotalLength();
                int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtTL_Ec_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbOtherSignature.isSelected()) {
                if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtTL_Ec_result)) {
                    ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingEuclideanDistance_ByTotalLength();
                    int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                    txtTL_Ec_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
                }
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingEuclideanDistance_ByTotalLength() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingEuclideanDistance_ByTotalLength() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingEuclideanDistance_ByTotalLength() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {

            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByTotalLengthUsingEuclidean(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    //endregion

    //endregion

    //region Distance between points

    // region Manhattan Distance

    public void btn_DP_MtClick() {
        if (CheckAndSetErrorOrTimeThresholdValue(txtDP_Mt_error.getText(), false, txtDP_Mt_result)) {
            if (rbGenuineSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingManhattanDistance_ByDistanceBetweenPoints();
                int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
                txtDP_Mt_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbForgedSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingManhattanDistance_ByDistanceBetweenPoints();
                int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtDP_Mt_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbOtherSignature.isSelected()) {
                if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtDP_Mt_result)) {
                    ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanDistance_ByDistanceBetweenPoints();
                    int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                    txtDP_Mt_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
                }
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingManhattanDistance_ByDistanceBetweenPoints() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingManhattanDistance_ByDistanceBetweenPoints() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanDistance_ByDistanceBetweenPoints() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingManhattanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    // endregion

    // region Euclidean Distance

    public void btn_DP_EcClick() {
        if (CheckAndSetErrorOrTimeThresholdValue(txtDP_Ec_error.getText(), false, txtDP_Ec_result)) {
            if (rbGenuineSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingEuclideanDistance_ByDistanceBetweenPoints();
                int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
                txtDP_Ec_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbForgedSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingEuclideanDistance_ByDistanceBetweenPoints();
                int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtDP_Ec_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbOtherSignature.isSelected()) {
                if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtDP_Ec_result)) {
                    ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingEuclideanDistance_ByDistanceBetweenPoints();
                    int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                    txtDP_Ec_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
                }
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingEuclideanDistance_ByDistanceBetweenPoints() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingEuclideanDistance_ByDistanceBetweenPoints() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingEuclideanDistance_ByDistanceBetweenPoints() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold));
        }
        return resultsOfVerification;
    }

    //endregion

    // region Minkowski Distance

    public void btn_DP_MkClick() {
        if (CheckAndSetErrorOrTimeThresholdValue(txtDP_Mk_error.getText(), false, txtDP_Mk_result)) {
            if (rbGenuineSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingMinkowskiDistance_ByDistanceBetweenPoints();
                int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
                txtDP_Mk_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbForgedSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingMinkowskiDistance_ByDistanceBetweenPoints();
                int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtDP_Mk_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbOtherSignature.isSelected()) {
                if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtDP_Mk_result)) {
                    ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingMinkowskiDistance_ByDistanceBetweenPoints();
                    int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                    txtDP_Mk_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
                }
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingMinkowskiDistance_ByDistanceBetweenPoints() {
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold, p, r));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingMinkowskiDistance_ByDistanceBetweenPoints() {
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold, p, r));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingMinkowskiDistance_ByDistanceBetweenPoints() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {

            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByDistanceBetweenPointsUsingMinkowskiDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), errorThreshold, p, r));
        }
        return resultsOfVerification;
    }

    //endregion

    //endregion

    //region Speed of writing

    //region Without time threshold

    public void btn_SWClick() {
        if (CheckAndSetErrorOrTimeThresholdValue(txtSW_error.getText(), false, txtSW_result)) {
            if (rbGenuineSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingHistogramsOfSpeed_WithoutTimeThreshold();
                int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
                txtSW_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbForgedSignature.isSelected()) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingHistogramOfSpeed_WithoutTimeThreshold();
                int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtSW_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
            }
            if (rbOtherSignature.isSelected()) {
                if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtSW_result)) {
                    ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingHistogramOfSpeed_WithoutTimeThreshold();
                    int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                    txtSW_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
                }
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingHistogramsOfSpeed_WithoutTimeThreshold() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithoutTimeThreshold
                    (modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingHistogramOfSpeed_WithoutTimeThreshold() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithoutTimeThreshold
                    (modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(), errorThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingHistogramOfSpeed_WithoutTimeThreshold() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithoutTimeThreshold
                    (modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(), errorThreshold));
        }
        return resultsOfVerification;
    }

    //endregion

    //region With time threshold

    public void btn_SW_TTClick() {
        if (CheckAndSetErrorOrTimeThresholdValue(txtSW_TT_error.getText(), false, txtSW_TT_result)) {
            if (CheckAndSetErrorOrTimeThresholdValue(txtSW_TT_timeError.getText(), true, txtSW_TT_result)) {
                if (rbGenuineSignature.isSelected()) {
                    ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingHistogramsOfSpeed_WithTimeThreshold();
                    int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
                    txtSW_TT_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
                }
                if (rbForgedSignature.isSelected()) {
                    ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingHistogramOfSpeed_WithTimeThreshold();
                    int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
                    txtSW_TT_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
                }
                if (rbOtherSignature.isSelected()) {
                    if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtSW_TT_result)) {
                        ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingHistogramOfSpeed_WithTimeThreshold();
                        int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                        txtSW_TT_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
                    }
                }
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingHistogramsOfSpeed_WithTimeThreshold() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), errorThreshold, timeThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingHistogramOfSpeed_WithTimeThreshold() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), errorThreshold, timeThreshold));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingHistogramOfSpeed_WithTimeThreshold() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.CheckIsTheSameSignatureByHistogramsOfSpeedUsingEuclideanDistanceWithTimeThreshold(modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation(), errorThreshold, timeThreshold));
        }
        return resultsOfVerification;
    }

    //endregion

    //endregion

    //region Total verifiers

    //region Total length using Manhattan distance and speed of writing Euclidean Distance

    public void btn_TV_MtClick() {
        if (rbGenuineSignature.isSelected()) {
            ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingManhattanAndEuclideanDistance_ByTotalVerifiers();
            int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
            txtTV_Mt_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
        }
        if (rbForgedSignature.isSelected()) {
            ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingManhattanAndEuclideanDistance_ByTotalVerifiers();
            int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
            txtTV_Mt_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
        }
        if (rbOtherSignature.isSelected()) {
            if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtTV_Mt_result)) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanAndEuclideanDistance_ByTotalVerifiers();
                int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtTV_Mt_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingManhattanAndEuclideanDistance_ByTotalVerifiers() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingManhattanAndEuclideanDistance_ByTotalVerifiers() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingManhattanAndEuclideanDistance_ByTotalVerifiers() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
        }
        return resultsOfVerification;
    }

    //endregion

    // region Total length using Euclidean distance and speed of writing Euclidean Distance

    public void btn_TV_EcClick() {
        if (rbGenuineSignature.isSelected()) {
            ArrayList<Boolean> resultsOfVerification = ReturnTrue_IfTestedSignatureIsOriginal_UsingOnlyEuclideanDistance_ByTotalVerifiers();
            int correctResultsOfTest = CountCorrectResultsOfTest(resultsOfVerification);
            txtTV_Ec_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
        }
        if (rbForgedSignature.isSelected()) {
            ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsForged_UsingOnlyEuclideanDistance_ByTotalVerifiers();
            int correctResultsOfTest = numberOfUsers - CountCorrectResultsOfTest(resultsOfVerification);
            txtTV_Ec_result.setText(String.format("Correct tests: %s/20", correctResultsOfTest));
        }
        if (rbOtherSignature.isSelected()) {
            if (CheckIfAuthenticatedUserIsTheSameAsOtherUser(txtTV_Ec_result)) {
                ArrayList<Boolean> resultsOfVerification = ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingOnlyEuclideanDistance_ByTotalVerifiers();
                int correctResultsOfTest = allUsers - CountCorrectResultsOfTest(resultsOfVerification);
                txtTV_Ec_result.setText(String.format("Correct tests: %s/40", correctResultsOfTest));
            }
        }
    }

    private ArrayList<Boolean> ReturnTrue_IfTestedSignatureIsOriginal_UsingOnlyEuclideanDistance_ByTotalVerifiers() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= numberOfUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsForged_UsingOnlyEuclideanDistance_ByTotalVerifiers() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 21; i <= allUsers; i++) {

            SignatureManipulation signatureToVerify = new SignatureManipulation(authenticatedUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
        }
        return resultsOfVerification;
    }

    private ArrayList<Boolean> ReturnFalse_IfTestedSignatureIsFromOtherUser_UsingOnlyEuclideanDistance_ByTotalVerifiers() {

        SignatureManipulation modelSignature = new SignatureManipulation(authenticatedUser, numberOfUsers);
        ArrayList<Boolean> resultsOfVerification = new ArrayList<>();

        for (int i = 1; i <= allUsers; i++) {
            SignatureManipulation signatureToVerify = new SignatureManipulation(otherUser, i, modelSignature.GetMaxSignatureLength());

            resultsOfVerification.add(SignatureVerifier.VerifySignatureUsingOnlyEuclideanDistance(modelSignature.GetModelAvgSignatureX(), modelSignature.GetModelAvgSignatureY(),
                    signatureToVerify.GetSignatureToVerifyX(), signatureToVerify.GetSignatureToVerifyY(), modelSignature.GetModelAvgSpeedOfSignature(), signatureToVerify.GetSpeedOfSignatureToVerify(),
                    modelSignature.GetMaxTimeOfSignatureCreation(), modelSignature.GetMinTimeOfSignatureCreation()));
        }
        return resultsOfVerification;
    }

    //endregion

    //endregion
}
