package hsx77;

public class SignatureSamplesContainer {

    //region Private Fields

    //region Original Samples

    //region Samples 1-10

    private double[] UXS1;
    private double[] UYS1;
    private double[] USS1;

    private double[] UXS2;
    private double[] UYS2;
    private double[] USS2;

    private double[] UXS3;
    private double[] UYS3;
    private double[] USS3;

    private double[] UXS4;
    private double[] UYS4;
    private double[] USS4;

    private double[] UXS5;
    private double[] UYS5;
    private double[] USS5;

    private double[] UXS6;
    private double[] UYS6;
    private double[] USS6;

    private double[] UXS7;
    private double[] UYS7;
    private double[] USS7;

    private double[] UXS8;
    private double[] UYS8;
    private double[] USS8;

    private double[] UXS9;
    private double[] UYS9;
    private double[] USS9;

    private double[] UXS10;
    private double[] UYS10;
    private double[] USS10;

    //endregion

    //region Samples 11-20

    private double[] UXS11;
    private double[] UYS11;
    private double[] USS11;

    private double[] UXS12;
    private double[] UYS12;
    private double[] USS12;

    private double[] UXS13;
    private double[] UYS13;
    private double[] USS13;

    private double[] UXS14;
    private double[] UYS14;
    private double[] USS14;

    private double[] UXS15;
    private double[] UYS15;
    private double[] USS15;

    private double[] UXS16;
    private double[] UYS16;
    private double[] USS16;

    private double[] UXS17;
    private double[] UYS17;
    private double[] USS17;

    private double[] UXS18;
    private double[] UYS18;
    private double[] USS18;

    private double[] UXS19;
    private double[] UYS19;
    private double[] USS19;

    private double[] UXS20;
    private double[] UYS20;
    private double[] USS20;

    //endregion

    //endregion

    //region Resampled Samples

    //region Resampled Samples 1-10

    private double[] RUXS1;
    private double[] RUYS1;
    private double[] RUSS1;

    private double[] RUXS2;
    private double[] RUYS2;
    private double[] RUSS2;

    private double[] RUXS3;
    private double[] RUYS3;
    private double[] RUSS3;

    private double[] RUXS4;
    private double[] RUYS4;
    private double[] RUSS4;

    private double[] RUXS5;
    private double[] RUYS5;
    private double[] RUSS5;

    private double[] RUXS6;
    private double[] RUYS6;
    private double[] RUSS6;

    private double[] RUXS7;
    private double[] RUYS7;
    private double[] RUSS7;

    private double[] RUXS8;
    private double[] RUYS8;
    private double[] RUSS8;

    private double[] RUXS9;
    private double[] RUYS9;
    private double[] RUSS9;

    private double[] RUXS10;
    private double[] RUYS10;
    private double[] RUSS10;

    //endregion

    //region  Resampled Samples 11-20

    private double[] RUXS11;
    private double[] RUYS11;
    private double[] RUSS11;

    private double[] RUXS12;
    private double[] RUYS12;
    private double[] RUSS12;

    private double[] RUXS13;
    private double[] RUYS13;
    private double[] RUSS13;

    private double[] RUXS14;
    private double[] RUYS14;
    private double[] RUSS14;

    private double[] RUXS15;
    private double[] RUYS15;
    private double[] RUSS15;

    private double[] RUXS16;
    private double[] RUYS16;
    private double[] RUSS16;

    private double[] RUXS17;
    private double[] RUYS17;
    private double[] RUSS17;

    private double[] RUXS18;
    private double[] RUYS18;
    private double[] RUSS18;

    private double[] RUXS19;
    private double[] RUYS19;
    private double[] RUSS19;

    private double[] RUXS20;
    private double[] RUYS20;
    private double[] RUSS20;

//endregion

    //endregion

    //endregion Fields

    // region Public Methods

    //region Original Samples

    public double[] GetXSignatureSample(int xSample) {
        switch (xSample) {
            case 1:
                return getUXS1();
            case 2:
                return getUXS2();
            case 3:
                return getUXS3();
            case 4:
                return getUXS4();
            case 5:
                return getUXS5();
            case 6:
                return getUXS6();
            case 7:
                return getUXS7();
            case 8:
                return getUXS8();
            case 9:
                return getUXS9();
            case 10:
                return getUXS10();
            case 11:
                return getUXS11();
            case 12:
                return getUXS12();
            case 13:
                return getUXS13();
            case 14:
                return getUXS14();
            case 15:
                return getUXS15();
            case 16:
                return getUXS16();
            case 17:
                return getUXS17();
            case 18:
                return getUXS18();
            case 19:
                return getUXS19();
            case 20:
                return getUXS20();
            default: {
                System.out.println("Invalid X sample.");
                return new double[0];
            }
        }
    }

    public double[] GetYSignatureSample(int ySample) {
        switch (ySample) {
            case 1:
                return getUYS1();
            case 2:
                return getUYS2();
            case 3:
                return getUYS3();
            case 4:
                return getUYS4();
            case 5:
                return getUYS5();
            case 6:
                return getUYS6();
            case 7:
                return getUYS7();
            case 8:
                return getUYS8();
            case 9:
                return getUYS9();
            case 10:
                return getUYS10();
            case 11:
                return getUYS11();
            case 12:
                return getUYS12();
            case 13:
                return getUYS13();
            case 14:
                return getUYS14();
            case 15:
                return getUYS15();
            case 16:
                return getUYS16();
            case 17:
                return getUYS17();
            case 18:
                return getUYS18();
            case 19:
                return getUYS19();
            case 20:
                return getUYS20();
            default: {
                System.out.println("Invalid Y sample.");
                return new double[0];
            }
        }
    }

    public double[] GetSpeedOfSignatureSample(int sSample) {
        switch (sSample) {
            case 1:
                return getUSS1();
            case 2:
                return getUSS2();
            case 3:
                return getUSS3();
            case 4:
                return getUSS4();
            case 5:
                return getUSS5();
            case 6:
                return getUSS6();
            case 7:
                return getUSS7();
            case 8:
                return getUSS8();
            case 9:
                return getUSS9();
            case 10:
                return getUSS10();
            case 11:
                return getUSS11();
            case 12:
                return getUSS12();
            case 13:
                return getUSS13();
            case 14:
                return getUSS14();
            case 15:
                return getUSS15();
            case 16:
                return getUSS16();
            case 17:
                return getUSS17();
            case 18:
                return getUSS18();
            case 19:
                return getUSS19();
            case 20:
                return getUSS20();
            default: {
                System.out.println("Invalid speed sample.");
                return new double[0];
            }
        }
    }

    public void SetXSignatureSample(int xSample, double[] xValues) {
        switch (xSample) {
            case 1:
                setUXS1(xValues);
                break;
            case 2:
                setUXS2(xValues);
                break;
            case 3:
                setUXS3(xValues);
                break;
            case 4:
                setUXS4(xValues);
                break;
            case 5:
                setUXS5(xValues);
                break;
            case 6:
                setUXS6(xValues);
                break;
            case 7:
                setUXS7(xValues);
                break;
            case 8:
                setUXS8(xValues);
                break;
            case 9:
                setUXS9(xValues);
                break;
            case 10:
                setUXS10(xValues);
                break;
            case 11:
                setUXS11(xValues);
                break;
            case 12:
                setUXS12(xValues);
                break;
            case 13:
                setUXS13(xValues);
                break;
            case 14:
                setUXS14(xValues);
                break;
            case 15:
                setUXS15(xValues);
                break;
            case 16:
                setUXS16(xValues);
                break;
            case 17:
                setUXS17(xValues);
                break;
            case 18:
                setUXS18(xValues);
                break;
            case 19:
                setUXS19(xValues);
                break;
            case 20:
                setUXS20(xValues);
                break;
            default:
                System.out.println("Invalid sample.");
                break;
        }
    }

    public void SetYSignatureSample(int ySample, double[] yValues) {
        switch (ySample) {
            case 1:
                setUYS1(yValues);
                break;
            case 2:
                setUYS2(yValues);
                break;
            case 3:
                setUYS3(yValues);
                break;
            case 4:
                setUYS4(yValues);
                break;
            case 5:
                setUYS5(yValues);
                break;
            case 6:
                setUYS6(yValues);
                break;
            case 7:
                setUYS7(yValues);
                break;
            case 8:
                setUYS8(yValues);
                break;
            case 9:
                setUYS9(yValues);
                break;
            case 10:
                setUYS10(yValues);
                break;
            case 11:
                setUYS11(yValues);
                break;
            case 12:
                setUYS12(yValues);
                break;
            case 13:
                setUYS13(yValues);
                break;
            case 14:
                setUYS14(yValues);
                break;
            case 15:
                setUYS15(yValues);
                break;
            case 16:
                setUYS16(yValues);
                break;
            case 17:
                setUYS17(yValues);
                break;
            case 18:
                setUYS18(yValues);
                break;
            case 19:
                setUYS19(yValues);
                break;
            case 20:
                setUYS20(yValues);
                break;
            default:
                System.out.println("Invalid sample.");
                break;
        }
    }

    public void SetSpeedOfSignatureSample(int speedSample, double[] speedValues) {
        switch (speedSample) {
            case 1:
                setUSS1(speedValues);
                break;
            case 2:
                setUSS2(speedValues);
                break;
            case 3:
                setUSS3(speedValues);
                break;
            case 4:
                setUSS4(speedValues);
                break;
            case 5:
                setUSS5(speedValues);
                break;
            case 6:
                setUSS6(speedValues);
                break;
            case 7:
                setUSS7(speedValues);
                break;
            case 8:
                setUSS8(speedValues);
                break;
            case 9:
                setUSS9(speedValues);
                break;
            case 10:
                setUSS10(speedValues);
                break;
            case 11:
                setUSS11(speedValues);
                break;
            case 12:
                setUSS12(speedValues);
                break;
            case 13:
                setUSS13(speedValues);
                break;
            case 14:
                setUSS14(speedValues);
                break;
            case 15:
                setUSS15(speedValues);
                break;
            case 16:
                setUSS16(speedValues);
                break;
            case 17:
                setUSS17(speedValues);
                break;
            case 18:
                setUSS18(speedValues);
                break;
            case 19:
                setUSS19(speedValues);
                break;
            case 20:
                setUSS20(speedValues);
                break;
            default:
                System.out.println("Invalid speed sample.");
                break;
        }
    }

    //endregion Original Samples

    //region Resampled Samples

    public double[] GetResampledXSignatureSample(int xSample) {
        switch (xSample) {
            case 1:
                return getRUXS1();
            case 2:
                return getRUXS2();
            case 3:
                return getRUXS3();
            case 4:
                return getRUXS4();
            case 5:
                return getRUXS5();
            case 6:
                return getRUXS6();
            case 7:
                return getRUXS7();
            case 8:
                return getRUXS8();
            case 9:
                return getRUXS9();
            case 10:
                return getRUXS10();
            case 11:
                return getRUXS11();
            case 12:
                return getRUXS12();
            case 13:
                return getRUXS13();
            case 14:
                return getRUXS14();
            case 15:
                return getRUXS15();
            case 16:
                return getRUXS16();
            case 17:
                return getRUXS17();
            case 18:
                return getRUXS18();
            case 19:
                return getRUXS19();
            case 20:
                return getRUXS20();
            default: {
                System.out.println("Invalid X sample.");
                return new double[0];
            }
        }
    }

    public double[] GetResampledYSignatureSample(int ySample) {
        switch (ySample) {
            case 1:
                return getRUYS1();
            case 2:
                return getRUYS2();
            case 3:
                return getRUYS3();
            case 4:
                return getRUYS4();
            case 5:
                return getRUYS5();
            case 6:
                return getRUYS6();
            case 7:
                return getRUYS7();
            case 8:
                return getRUYS8();
            case 9:
                return getRUYS9();
            case 10:
                return getRUYS10();
            case 11:
                return getRUYS11();
            case 12:
                return getRUYS12();
            case 13:
                return getRUYS13();
            case 14:
                return getRUYS14();
            case 15:
                return getRUYS15();
            case 16:
                return getRUYS16();
            case 17:
                return getRUYS17();
            case 18:
                return getRUYS18();
            case 19:
                return getRUYS19();
            case 20:
                return getRUYS20();
            default: {
                System.out.println("Invalid Y sample.");
                return new double[0];
            }
        }
    }

    public double[] GetResampledSpeedOfSignatureSample(int speedSample) {
        switch (speedSample) {
            case 1:
                return getRUSS1();
            case 2:
                return getRUSS2();
            case 3:
                return getRUSS3();
            case 4:
                return getRUSS4();
            case 5:
                return getRUSS5();
            case 6:
                return getRUSS6();
            case 7:
                return getRUSS7();
            case 8:
                return getRUSS8();
            case 9:
                return getRUSS9();
            case 10:
                return getRUSS10();
            case 11:
                return getRUSS11();
            case 12:
                return getRUSS12();
            case 13:
                return getRUSS13();
            case 14:
                return getRUSS14();
            case 15:
                return getRUSS15();
            case 16:
                return getRUSS16();
            case 17:
                return getRUSS17();
            case 18:
                return getRUSS18();
            case 19:
                return getRUSS19();
            case 20:
                return getRUSS20();
            default: {
                System.out.println("Invalid S sample.");
                return new double[0];
            }
        }
    }

    public void SetResampledXSignatureSample(int sample, double[] xValues) {
        switch (sample) {
            case 1:
                setRUXS1(xValues);
                break;
            case 2:
                setRUXS2(xValues);
                break;
            case 3:
                setRUXS3(xValues);
                break;
            case 4:
                setRUXS4(xValues);
                break;
            case 5:
                setRUXS5(xValues);
                break;
            case 6:
                setRUXS6(xValues);
                break;
            case 7:
                setRUXS7(xValues);
                break;
            case 8:
                setRUXS8(xValues);
                break;
            case 9:
                setRUXS9(xValues);
                break;
            case 10:
                setRUXS10(xValues);
                break;
            case 11:
                setRUXS11(xValues);
                break;
            case 12:
                setRUXS12(xValues);
                break;
            case 13:
                setRUXS13(xValues);
                break;
            case 14:
                setRUXS14(xValues);
                break;
            case 15:
                setRUXS15(xValues);
                break;
            case 16:
                setRUXS16(xValues);
                break;
            case 17:
                setRUXS17(xValues);
                break;
            case 18:
                setRUXS18(xValues);
                break;
            case 19:
                setRUXS19(xValues);
                break;
            case 20:
                setRUXS20(xValues);
                break;
            default:
                System.out.println("Invalid X sample.");
                break;
        }
    }

    public void SetResampledYSignatureSample(int ySample, double[] yValues) {
        switch (ySample) {
            case 1:
                setRUYS1(yValues);
                break;
            case 2:
                setRUYS2(yValues);
                break;
            case 3:
                setRUYS3(yValues);
                break;
            case 4:
                setRUYS4(yValues);
                break;
            case 5:
                setRUYS5(yValues);
                break;
            case 6:
                setRUYS6(yValues);
                break;
            case 7:
                setRUYS7(yValues);
                break;
            case 8:
                setRUYS8(yValues);
                break;
            case 9:
                setRUYS9(yValues);
                break;
            case 10:
                setRUYS10(yValues);
                break;
            case 11:
                setRUYS11(yValues);
                break;
            case 12:
                setRUYS12(yValues);
                break;
            case 13:
                setRUYS13(yValues);
                break;
            case 14:
                setRUYS14(yValues);
                break;
            case 15:
                setRUYS15(yValues);
                break;
            case 16:
                setRUYS16(yValues);
                break;
            case 17:
                setRUYS17(yValues);
                break;
            case 18:
                setRUYS18(yValues);
                break;
            case 19:
                setRUYS19(yValues);
                break;
            case 20:
                setRUYS20(yValues);
                break;
            default:
                System.out.println("Invalid Y sample.");
                break;
        }
    }

    public void SetResampledSpeedOfSignatureSample(int speedSample, double[] speedValues) {
        switch (speedSample) {
            case 1:
                setRUSS1(speedValues);
                break;
            case 2:
                setRUSS2(speedValues);
                break;
            case 3:
                setRUSS3(speedValues);
                break;
            case 4:
                setRUSS4(speedValues);
                break;
            case 5:
                setRUSS5(speedValues);
                break;
            case 6:
                setRUSS6(speedValues);
                break;
            case 7:
                setRUSS7(speedValues);
                break;
            case 8:
                setRUSS8(speedValues);
                break;
            case 9:
                setRUSS9(speedValues);
                break;
            case 10:
                setRUSS10(speedValues);
                break;
            case 11:
                setRUSS11(speedValues);
                break;
            case 12:
                setRUSS12(speedValues);
                break;
            case 13:
                setRUSS13(speedValues);
                break;
            case 14:
                setRUSS14(speedValues);
                break;
            case 15:
                setRUSS15(speedValues);
                break;
            case 16:
                setRUSS16(speedValues);
                break;
            case 17:
                setRUSS17(speedValues);
                break;
            case 18:
                setRUSS18(speedValues);
                break;
            case 19:
                setRUSS19(speedValues);
                break;
            case 20:
                setRUSS20(speedValues);
                break;
            default:
                System.out.println("Invalid speed sample.");
                break;
        }
    }

    //endregion Resampled Samples

    // endregion

    //region Private Properties

    //region Original Samples Properties

    //region Properties X

    //region Samples 1-10

    private double[] getUXS1() {
        return this.UXS1;
    }

    private void setUXS1(double[] value) {
        this.UXS1 = value;
    }

    private double[] getUXS2() {
        return this.UXS2;
    }

    private void setUXS2(double[] value) {
        this.UXS2 = value;
    }

    private double[] getUXS3() {
        return this.UXS3;
    }

    private void setUXS3(double[] value) {
        this.UXS3 = value;
    }

    private double[] getUXS4() {
        return this.UXS4;
    }

    private void setUXS4(double[] value) {
        this.UXS4 = value;
    }

    private double[] getUXS5() {
        return this.UXS5;
    }

    private void setUXS5(double[] value) {
        this.UXS5 = value;
    }

    private double[] getUXS6() {
        return this.UXS6;
    }

    private void setUXS6(double[] value) {
        this.UXS6 = value;
    }

    private double[] getUXS7() {
        return this.UXS7;
    }

    private void setUXS7(double[] value) {
        this.UXS7 = value;
    }

    private double[] getUXS8() {
        return this.UXS8;
    }

    private void setUXS8(double[] value) {
        this.UXS8 = value;
    }

    private double[] getUXS9() {
        return this.UXS9;
    }

    private void setUXS9(double[] value) {
        this.UXS9 = value;
    }

    private double[] getUXS10() {
        return this.UXS10;
    }

    private void setUXS10(double[] value) {
        this.UXS10 = value;
    }

    //endregion

    //region Samples 11-20

    private double[] getUXS11() {
        return this.UXS11;
    }

    private void setUXS11(double[] value) {
        this.UXS11 = value;
    }

    private double[] getUXS12() {
        return this.UXS12;
    }

    private void setUXS12(double[] value) {
        this.UXS12 = value;
    }

    private double[] getUXS13() {
        return this.UXS13;
    }

    private void setUXS13(double[] value) {
        this.UXS13 = value;
    }

    private double[] getUXS14() {
        return this.UXS14;
    }

    private void setUXS14(double[] value) {
        this.UXS14 = value;
    }

    private double[] getUXS15() {
        return this.UXS15;
    }

    private void setUXS15(double[] value) {
        this.UXS15 = value;
    }

    private double[] getUXS16() {
        return this.UXS16;
    }

    private void setUXS16(double[] value) {
        this.UXS16 = value;
    }

    private double[] getUXS17() {
        return this.UXS17;
    }

    private void setUXS17(double[] value) {
        this.UXS17 = value;
    }

    private double[] getUXS18() {
        return this.UXS18;
    }

    private void setUXS18(double[] value) {
        this.UXS18 = value;
    }

    private double[] getUXS19() {
        return this.UXS19;
    }

    private void setUXS19(double[] value) {
        this.UXS19 = value;
    }

    private double[] getUXS20() {
        return this.UXS20;
    }

    private void setUXS20(double[] value) {
        this.UXS20 = value;
    }

    //endregion

    //endregion X

    //region Properties Y

    //region Samples 1-10

    private double[] getUYS1() {
        return this.UYS1;
    }

    private void setUYS1(double[] value) {
        this.UYS1 = value;
    }

    private double[] getUYS2() {
        return this.UYS2;
    }

    private void setUYS2(double[] value) {
        this.UYS2 = value;
    }

    private double[] getUYS3() {
        return this.UYS3;
    }

    private void setUYS3(double[] value) {
        this.UYS3 = value;
    }

    private double[] getUYS4() {
        return this.UYS4;
    }

    private void setUYS4(double[] value) {
        this.UYS4 = value;
    }

    private double[] getUYS5() {
        return this.UYS5;
    }

    private void setUYS5(double[] value) {
        this.UYS5 = value;
    }

    private double[] getUYS6() {
        return this.UYS6;
    }

    private void setUYS6(double[] value) {
        this.UYS6 = value;
    }

    private double[] getUYS7() {
        return this.UYS7;
    }

    private void setUYS7(double[] value) {
        this.UYS7 = value;
    }

    private double[] getUYS8() {
        return this.UYS8;
    }

    private void setUYS8(double[] value) {
        this.UYS8 = value;
    }

    private double[] getUYS9() {
        return this.UYS9;
    }

    private void setUYS9(double[] value) {
        this.UYS9 = value;
    }

    private double[] getUYS10() {
        return this.UYS10;
    }

    private void setUYS10(double[] value) {
        this.UYS10 = value;
    }

    //endregion

    //region Samples 11-20

    private double[] getUYS11() {
        return this.UYS11;
    }

    private void setUYS11(double[] value) {
        this.UYS11 = value;
    }

    private double[] getUYS12() {
        return this.UYS12;
    }

    private void setUYS12(double[] value) {
        this.UYS12 = value;
    }

    private double[] getUYS13() {
        return this.UYS13;
    }

    private void setUYS13(double[] value) {
        this.UYS13 = value;
    }

    private double[] getUYS14() {
        return this.UYS14;
    }

    private void setUYS14(double[] value) {
        this.UYS14 = value;
    }

    private double[] getUYS15() {
        return this.UYS15;
    }

    private void setUYS15(double[] value) {
        this.UYS15 = value;
    }

    private double[] getUYS16() {
        return this.UYS16;
    }

    private void setUYS16(double[] value) {
        this.UYS16 = value;
    }

    private double[] getUYS17() {
        return this.UYS17;
    }

    private void setUYS17(double[] value) {
        this.UYS17 = value;
    }

    private double[] getUYS18() {
        return this.UYS18;
    }

    private void setUYS18(double[] value) {
        this.UYS18 = value;
    }

    private double[] getUYS19() {
        return this.UYS19;
    }

    private void setUYS19(double[] value) {
        this.UYS19 = value;
    }

    private double[] getUYS20() {
        return this.UYS20;
    }

    private void setUYS20(double[] value) {
        this.UYS20 = value;
    }

    //endregion Y

    //endregion Original Samples

    //region Properties S

    //region Samples 1-10

    private double[] getUSS1() {
        return this.USS1;
    }

    private void setUSS1(double[] value) {
        this.USS1 = value;
    }

    private double[] getUSS2() {
        return this.USS2;
    }

    private void setUSS2(double[] value) {
        this.USS2 = value;
    }

    private double[] getUSS3() {
        return this.USS3;
    }

    private void setUSS3(double[] value) {
        this.USS3 = value;
    }

    private double[] getUSS4() {
        return this.USS4;
    }

    private void setUSS4(double[] value) {
        this.USS4 = value;
    }

    private double[] getUSS5() {
        return this.USS5;
    }

    private void setUSS5(double[] value) {
        this.USS5 = value;
    }

    private double[] getUSS6() {
        return this.USS6;
    }

    private void setUSS6(double[] value) {
        this.USS6 = value;
    }

    private double[] getUSS7() {
        return this.USS7;
    }

    private void setUSS7(double[] value) {
        this.USS7 = value;
    }

    private double[] getUSS8() {
        return this.USS8;
    }

    private void setUSS8(double[] value) {
        this.USS8 = value;
    }

    private double[] getUSS9() {
        return this.USS9;
    }

    private void setUSS9(double[] value) {
        this.USS9 = value;
    }

    private double[] getUSS10() {
        return this.USS10;
    }

    private void setUSS10(double[] value) {
        this.USS10 = value;
    }

    //endregion

    //region Samples 11-20

    private double[] getUSS11() {
        return this.USS11;
    }

    private void setUSS11(double[] value) {
        this.USS11 = value;
    }

    private double[] getUSS12() {
        return this.USS12;
    }

    private void setUSS12(double[] value) {
        this.USS12 = value;
    }

    private double[] getUSS13() {
        return this.USS13;
    }

    private void setUSS13(double[] value) {
        this.USS13 = value;
    }

    private double[] getUSS14() {
        return this.USS14;
    }

    private void setUSS14(double[] value) {
        this.USS14 = value;
    }

    private double[] getUSS15() {
        return this.USS15;
    }

    private void setUSS15(double[] value) {
        this.USS15 = value;
    }

    private double[] getUSS16() {
        return this.USS16;
    }

    private void setUSS16(double[] value) {
        this.USS16 = value;
    }

    private double[] getUSS17() {
        return this.USS17;
    }

    private void setUSS17(double[] value) {
        this.USS17 = value;
    }

    private double[] getUSS18() {
        return this.USS18;
    }

    private void setUSS18(double[] value) {
        this.USS18 = value;
    }

    private double[] getUSS19() {
        return this.USS19;
    }

    private void setUSS19(double[] value) {
        this.USS19 = value;
    }

    private double[] getUSS20() {
        return this.USS20;
    }

    private void setUSS20(double[] value) {
        this.USS20 = value;
    }

    //endregion Samples 11-20

    //endregion Properties S

    //endregion Original Samples Properties

    //region Resampled Samples Properties

    //region Properties X

    //region Samples 1-10

    private double[] getRUXS1() {
        return this.RUXS1;
    }

    private void setRUXS1(double[] value) {
        this.RUXS1 = value;
    }

    private double[] getRUXS2() {
        return this.RUXS2;
    }

    private void setRUXS2(double[] value) {
        this.RUXS2 = value;
    }

    private double[] getRUXS3() {
        return this.RUXS3;
    }

    private void setRUXS3(double[] value) {
        this.RUXS3 = value;
    }

    private double[] getRUXS4() {
        return this.RUXS4;
    }

    private void setRUXS4(double[] value) {
        this.RUXS4 = value;
    }

    private double[] getRUXS5() {
        return this.RUXS5;
    }

    private void setRUXS5(double[] value) {
        this.RUXS5 = value;
    }

    private double[] getRUXS6() {
        return this.RUXS6;
    }

    private void setRUXS6(double[] value) {
        this.RUXS6 = value;
    }

    private double[] getRUXS7() {
        return this.RUXS7;
    }

    private void setRUXS7(double[] value) {
        this.RUXS7 = value;
    }

    private double[] getRUXS8() {
        return this.RUXS8;
    }

    private void setRUXS8(double[] value) {
        this.RUXS8 = value;
    }

    private double[] getRUXS9() {
        return this.RUXS9;
    }

    private void setRUXS9(double[] value) {
        this.RUXS9 = value;
    }

    private double[] getRUXS10() {
        return this.RUXS10;
    }

    private void setRUXS10(double[] value) {
        this.RUXS10 = value;
    }

    //endregion Samples 1-10

    //region Samples 11-20

    private double[] getRUXS11() {
        return this.RUXS11;
    }

    private void setRUXS11(double[] value) {
        this.RUXS11 = value;
    }

    private double[] getRUXS12() {
        return this.RUXS12;
    }

    private void setRUXS12(double[] value) {
        this.RUXS12 = value;
    }

    private double[] getRUXS13() {
        return this.RUXS13;
    }

    private void setRUXS13(double[] value) {
        this.RUXS13 = value;
    }

    private double[] getRUXS14() {
        return this.RUXS14;
    }

    private void setRUXS14(double[] value) {
        this.RUXS14 = value;
    }

    private double[] getRUXS15() {
        return this.RUXS15;
    }

    private void setRUXS15(double[] value) {
        this.RUXS15 = value;
    }

    private double[] getRUXS16() {
        return this.RUXS16;
    }

    private void setRUXS16(double[] value) {
        this.RUXS16 = value;
    }

    private double[] getRUXS17() {
        return this.RUXS17;
    }

    private void setRUXS17(double[] value) {
        this.RUXS17 = value;
    }

    private double[] getRUXS18() {
        return this.RUXS18;
    }

    private void setRUXS18(double[] value) {
        this.RUXS18 = value;
    }

    private double[] getRUXS19() {
        return this.RUXS19;
    }

    private void setRUXS19(double[] value) {
        this.RUXS19 = value;
    }

    private double[] getRUXS20() {
        return this.RUXS20;
    }

    private void setRUXS20(double[] value) {
        this.RUXS20 = value;
    }

    //endregion Samples S11-S20

    //endregion

    //region Properties Y

    //region Samples 1-10

    private double[] getRUYS1() {
        return this.RUYS1;
    }

    private void setRUYS1(double[] value) {
        this.RUYS1 = value;
    }

    private double[] getRUYS2() {
        return this.RUYS2;
    }

    private void setRUYS2(double[] value) {
        this.RUYS2 = value;
    }

    private double[] getRUYS3() {
        return this.RUYS3;
    }

    private void setRUYS3(double[] value) {
        this.RUYS3 = value;
    }

    private double[] getRUYS4() {
        return this.RUYS4;
    }

    private void setRUYS4(double[] value) {
        this.RUYS4 = value;
    }

    private double[] getRUYS5() {
        return this.RUYS5;
    }

    private void setRUYS5(double[] value) {
        this.RUYS5 = value;
    }

    private double[] getRUYS6() {
        return this.RUYS6;
    }

    private void setRUYS6(double[] value) {
        this.RUYS6 = value;
    }

    private double[] getRUYS7() {
        return this.RUYS7;
    }

    private void setRUYS7(double[] value) {
        this.RUYS7 = value;
    }

    private double[] getRUYS8() {
        return this.RUYS8;
    }

    private void setRUYS8(double[] value) {
        this.RUYS8 = value;
    }

    private double[] getRUYS9() {
        return this.RUYS9;
    }

    private void setRUYS9(double[] value) {
        this.RUYS9 = value;
    }

    private double[] getRUYS10() {
        return this.RUYS10;
    }

    private void setRUYS10(double[] value) {
        this.RUYS10 = value;
    }

    //endregion

    //region Samples 11-20

    private double[] getRUYS11() {
        return this.RUYS11;
    }

    private void setRUYS11(double[] value) {
        this.RUYS11 = value;
    }

    private double[] getRUYS12() {
        return this.RUYS12;
    }

    private void setRUYS12(double[] value) {
        this.RUYS12 = value;
    }

    private double[] getRUYS13() {
        return this.RUYS13;
    }

    private void setRUYS13(double[] value) {
        this.RUYS13 = value;
    }

    private double[] getRUYS14() {
        return this.RUYS14;
    }

    private void setRUYS14(double[] value) {
        this.RUYS14 = value;
    }

    private double[] getRUYS15() {
        return this.RUYS15;
    }

    private void setRUYS15(double[] value) {
        this.RUYS15 = value;
    }

    private double[] getRUYS16() {
        return this.RUYS16;
    }

    private void setRUYS16(double[] value) {
        this.RUYS16 = value;
    }

    private double[] getRUYS17() {
        return this.RUYS17;
    }

    private void setRUYS17(double[] value) {
        this.RUYS17 = value;
    }

    private double[] getRUYS18() {
        return this.RUYS18;
    }

    private void setRUYS18(double[] value) {
        this.RUYS18 = value;
    }

    private double[] getRUYS19() {
        return this.RUYS19;
    }

    private void setRUYS19(double[] value) {
        this.RUYS19 = value;
    }

    private double[] getRUYS20() {
        return this.RUYS20;
    }

    private void setRUYS20(double[] value) {
        this.RUYS20 = value;
    }

    //endregion

    //endregion Y

    //region Properties S

    //region Samples 1-10

    private double[] getRUSS1() {
        return this.RUSS1;
    }

    private void setRUSS1(double[] value) {
        this.RUSS1 = value;
    }

    private double[] getRUSS2() {
        return this.RUSS2;
    }

    private void setRUSS2(double[] value) {
        this.RUSS2 = value;
    }

    private double[] getRUSS3() {
        return this.RUSS3;
    }

    private void setRUSS3(double[] value) {
        this.RUSS3 = value;
    }

    private double[] getRUSS4() {
        return this.RUSS4;
    }

    private void setRUSS4(double[] value) {
        this.RUSS4 = value;
    }

    private double[] getRUSS5() {
        return this.RUSS5;
    }

    private void setRUSS5(double[] value) {
        this.RUSS5 = value;
    }

    private double[] getRUSS6() {
        return this.RUSS6;
    }

    private void setRUSS6(double[] value) {
        this.RUSS6 = value;
    }

    private double[] getRUSS7() {
        return this.RUSS7;
    }

    private void setRUSS7(double[] value) {
        this.RUSS7 = value;
    }

    private double[] getRUSS8() {
        return this.RUSS8;
    }

    private void setRUSS8(double[] value) {
        this.RUSS8 = value;
    }

    private double[] getRUSS9() {
        return this.RUSS9;
    }

    private void setRUSS9(double[] value) {
        this.RUSS9 = value;
    }

    private double[] getRUSS10() {
        return this.RUSS10;
    }

    private void setRUSS10(double[] value) {
        this.RUSS10 = value;
    }

    //endregion

    //region Samples 11-20

    private double[] getRUSS11() {
        return this.RUSS11;
    }

    private void setRUSS11(double[] value) {
        this.RUSS11 = value;
    }

    private double[] getRUSS12() {
        return this.RUSS12;
    }

    private void setRUSS12(double[] value) {
        this.RUSS12 = value;
    }

    private double[] getRUSS13() {
        return this.RUSS13;
    }

    private void setRUSS13(double[] value) {
        this.RUSS13 = value;
    }

    private double[] getRUSS14() {
        return this.RUSS14;
    }

    private void setRUSS14(double[] value) {
        this.RUSS14 = value;
    }

    private double[] getRUSS15() {
        return this.RUSS15;
    }

    private void setRUSS15(double[] value) {
        this.RUSS15 = value;
    }

    private double[] getRUSS16() {
        return this.RUSS16;
    }

    private void setRUSS16(double[] value) {
        this.RUSS16 = value;
    }

    private double[] getRUSS17() {
        return this.RUSS17;
    }

    private void setRUSS17(double[] value) {
        this.RUSS17 = value;
    }

    private double[] getRUSS18() {
        return this.RUSS18;
    }

    private void setRUSS18(double[] value) {
        this.RUSS18 = value;
    }

    private double[] getRUSS19() {
        return this.RUSS19;
    }

    private void setRUSS19(double[] value) {
        this.RUSS19 = value;
    }

    private double[] getRUSS20() {
        return this.RUSS20;
    }

    private void setRUSS20(double[] value) {
        this.RUSS20 = value;
    }

    //endregion

    //endregion S

    //endregion Resampled Samples Properties

    //endregion Private Properties
}